package com.skyhubdigital.assessment;
import java.io.IOException;
import java.math.BigDecimal;

import com.skyhubdigital.assessment.salestax.ShoppingCart;
import com.skyhubdigital.assessment.salestax.Tax.GoodsClassification;
import com.skyhubdigital.assessment.salestax.model.Category;
import com.skyhubdigital.assessment.salestax.model.Item;

/**
 * Generate the receipts based on the assessment data
 * @author Gaurav Wasan
 *
 */
public class SalesTaxTest {
    
    public static void main(String[] args) throws IOException {
    	System.out.println("OUTPUT");

    	///////////////////////////////////
    	System.out.println("");
    	System.out.println("Output 1:");
        ShoppingCart shoppingBasket1 = new ShoppingCart();
        Item book = new Item("book", new Category("book", GoodsClassification.DOMESTIC), new BigDecimal("1"), new BigDecimal("12.49")); 
        Item music = new Item("music CD", new Category("music", GoodsClassification.DOMESTIC), new BigDecimal("1"), new BigDecimal("14.99"));
        Item chocolateBar = new Item("chocolate bar", new Category("food", GoodsClassification.DOMESTIC),  new BigDecimal("1"), new BigDecimal("0.85"));
                
        shoppingBasket1.addShoppingItem(book);
        shoppingBasket1.addShoppingItem(music);
        shoppingBasket1.addShoppingItem(chocolateBar);
        System.out.println(book.getQuantity() + "   " + book.getName() + "                               " + book.getPrice().add(book.getTax()));
        System.out.println(music.getQuantity() + "   " + music.getName() + "                           " + music.getPrice().add(music.getTax()));
        System.out.println(chocolateBar.getQuantity() + "   " + chocolateBar.getName()+ "                      " + chocolateBar.getPrice().add(chocolateBar.getTax()));
        System.out.println("Sales taxes: " + shoppingBasket1.getSalesTax());
        System.out.println("Total: " + shoppingBasket1.getTotal());

        ///////////////////////////////////
        System.out.println();
        System.out.println("Output 2:");
        ShoppingCart shoppingBasket2 = new ShoppingCart();
        Item boxOdChocolates = new Item("imported box of chocolates", new Category("food", GoodsClassification.IMPORTED),  new BigDecimal("1"), new BigDecimal("10.00"));
        Item bottleOfPerfume = new Item("imported bottle of perfume", new Category("perfume", GoodsClassification.IMPORTED),  new BigDecimal("1"), new BigDecimal("47.50"));
        
        shoppingBasket2.addShoppingItem(boxOdChocolates);
        shoppingBasket2.addShoppingItem(bottleOfPerfume);
        System.out.println(boxOdChocolates.getQuantity() + "   " + boxOdChocolates.getName() + "         " + boxOdChocolates.getPrice().add(boxOdChocolates.getTax()));
        System.out.println(bottleOfPerfume.getQuantity() + "   " + bottleOfPerfume.getName() + "         " + bottleOfPerfume.getPrice().add(bottleOfPerfume.getTax()));
        System.out.println("Sales taxes: " + shoppingBasket2.getSalesTax());
        System.out.println("Total: " + shoppingBasket2.getTotal());

        ///////////////////////////////////
        System.out.println();
        System.out.println("Output 3:");
        ShoppingCart shoppingBasket3 = new ShoppingCart();
        Item bottleOfPerfume1 = new Item("imported bottle of perfume", new Category("perfume", GoodsClassification.IMPORTED), new BigDecimal("1"), new BigDecimal("27.99"));
        Item bottleOfPerfume2 = new Item("bottle of perfume", new Category("perfume", GoodsClassification.DOMESTIC), new BigDecimal("1"), new BigDecimal("18.99"));
        Item packetOfHeadachePills = new Item("packet of headache pills", new Category("medical", GoodsClassification.DOMESTIC), new BigDecimal("1"), new BigDecimal("9.75"));
        Item boxOfImportedChocolates = new Item("box of imported chocolates", new Category("food", GoodsClassification.IMPORTED), new BigDecimal("1"), new BigDecimal("11.25"));

        shoppingBasket3.addShoppingItem(bottleOfPerfume1);
        shoppingBasket3.addShoppingItem(bottleOfPerfume2);
        shoppingBasket3.addShoppingItem(packetOfHeadachePills);
        shoppingBasket3.addShoppingItem(boxOfImportedChocolates);
        System.out.println(bottleOfPerfume1.getQuantity() + "   " + bottleOfPerfume1.getName() + "         " + bottleOfPerfume1.getPrice().add(bottleOfPerfume1.getTax()));
        System.out.println(bottleOfPerfume2.getQuantity() + "   " + bottleOfPerfume2.getName() + "                  " + bottleOfPerfume2.getPrice().add(bottleOfPerfume2.getTax()));
        System.out.println(packetOfHeadachePills.getQuantity() + "   " + packetOfHeadachePills.getName() + "           " + packetOfHeadachePills.getPrice().add(packetOfHeadachePills.getTax()));
        System.out.println(boxOfImportedChocolates.getQuantity() + "   " + boxOfImportedChocolates.getName() + "         " + boxOfImportedChocolates.getPrice().add(boxOfImportedChocolates.getTax()));
        System.out.println("Sales taxes: " + shoppingBasket3.getSalesTax());
        System.out.println("Total: " + shoppingBasket3.getTotal());
    }
}