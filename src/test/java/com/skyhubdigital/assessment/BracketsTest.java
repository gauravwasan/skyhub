package com.skyhubdigital.assessment;

import junit.framework.TestCase;

import com.skyhubdigital.assessment.brackets.Brackets;

/**
 * Unit test for Brackets.
 * @author Gaurav Wasan
 *
 */
public class BracketsTest extends TestCase {
	public void testValid1() {
		boolean isValid = new Brackets()
				.validator("{aa [bbb ]cccc (ddddd )eeeee }fffff");
		assertTrue(isValid);
	}

	public void testValid2() {
		boolean isValid = new Brackets().validator("{ [ ] ( ) }");
		assertTrue(isValid);
	}

	public void testValidNoBrackets() {
		boolean isValid = new Brackets().validator("abcdrfghijkl");
		assertTrue(isValid);
	}
	
	/////////////////////////Invalid
	
	public void testInvalid1() {
		boolean isValid = new Brackets().validator("{ [ ( ] ) }");
		assertFalse(isValid);
	}

	public void testInvalid2() {
		boolean isValid = new Brackets().validator("} ] ) [ ( {");
		assertFalse(isValid);
	}

	public void testInvalidModulus() {
		boolean isValid = new Brackets().validator("{ [ }");
		assertFalse(isValid);
	}
}
