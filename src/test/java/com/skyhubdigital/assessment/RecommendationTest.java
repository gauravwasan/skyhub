package com.skyhubdigital.assessment;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import junit.framework.TestCase;

import com.skyhubdigital.assessment.recommendation.Recommendation;
import com.skyhubdigital.assessment.recommendation.model.Movie;

/**
 * Unit test for Recommendation.
 * @author Gaurav Wasan
 */
public class RecommendationTest extends TestCase {

	static Logger logger = Logger.getLogger(Recommendation.class.getName());
	// verify the output as in the assessment example
	public void testValid1() throws IOException, URISyntaxException {
		
		int flightTime = 92;
		Set<Movie> recommendedMovieSet = (new Recommendation(flightTime)).getRecommendation();
		logger.log(Level.INFO, "---------------------------------------------");
		logger.log(Level.INFO, recommendedMovieSet.toString());
		assertEquals(recommendedMovieSet.size(), 2);
	}

	// verify the output as in the assessment example
	public void testValid2() throws IOException, URISyntaxException {
		int flightTime = 325;
		Set<Movie> recommendedMovieSet = (new Recommendation(flightTime)).getRecommendation();
		logger.log(Level.INFO, "---------------------------------------------");
		logger.log(Level.INFO, recommendedMovieSet.toString());
		assertEquals(recommendedMovieSet.size(), 3);
	}
	
	// flight with 0 flight time gets no movies
	public void testValid3() throws IOException, URISyntaxException {
		int flightTime = 0;
		Set<Movie> recommendedMovieSet = (new Recommendation(flightTime)).getRecommendation();
		logger.log(Level.INFO, "---------------------------------------------");
		logger.log(Level.INFO, recommendedMovieSet.toString());
		assertEquals(recommendedMovieSet.size(), 0);
	}	
	
	// flight with negative flight time gets no movies
	public void testValid4() throws IOException, URISyntaxException {
		int flightTime = -1;
		Set<Movie> recommendedMovieSet = (new Recommendation(flightTime)).getRecommendation();
		logger.log(Level.INFO, "---------------------------------------------");
		logger.log(Level.INFO, recommendedMovieSet.toString());
		assertEquals(recommendedMovieSet.size(), 0);
	}
}