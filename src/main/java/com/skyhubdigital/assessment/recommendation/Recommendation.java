package com.skyhubdigital.assessment.recommendation;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.skyhubdigital.assessment.recommendation.model.Movie;
/**
Build an in-flight entertainment system with on-demand movie streaming.
Users on longer flights like to start another movie right when their previous one ends, but they complain that the plane usually lands before they can see the ending.

// IGNORED this requirement of only 2 movies as it contradicts with the the example having 3 movie.//
So you're building a feature for choosing two movies whose total runtimes will equal the exact flight length.

Write a program that suggests movies for a flight.
The movie list can be a hardcoded array of movies (or loaded from a database)

Remember:
Threat all durations in minutes
Don't make your users watch the same movie twice
Make sure the movies suggested are as close as possible to the flight length, but never longer
Prioritize longer movies over shorter ones (longer movies are always better right)
Don't worry about genres

Examples:
Flight duration 92 min: The Shallows (87 min)
Flight duration 325 min: Suicide Squad (130 min) Jason Bourne (123 min) Batman: The Killing Joke (72 min)

 * @author Gaurav Wasan
 *
 */
public class Recommendation {

	static Logger logger = Logger.getLogger(Recommendation.class.getName());
	private int flightTime;

	public Recommendation(int flightTime) throws IOException, URISyntaxException
	{
		this.flightTime = flightTime;
	}
	
	/**
	 * The function that returns the recommended list of movies to the caller Prioritizing longer movies ahead of shorter ones.
	 * @return recommendedMovieSet
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public Set<Movie> getRecommendation() throws IOException, URISyntaxException
	{
		 Gson gson = new Gson();
		 Set<Movie> recommendedMovieSet = new TreeSet<Movie>();

		 // 1) Load the movies as a set for the req. 'Don't make your users watch the same movie twice'. Incase there are duplicates in the json file.
		 // SortedSet for req. 'Prioritize longer movies over shorter ones (longer movies are always better right)'
		Set<Movie> movieSet = gson.fromJson(getMovies(), new TypeToken<SortedSet<Movie>>(){}.getType());
		
		// movies in descending order of their runtime
		logger.log(Level.INFO, String.format("Available movie set %s", movieSet));
		
		// 2) populate the recommendedMovieSet starting as close as possible to the flight length
		for (Movie movie : movieSet) {
			// 3) If there is no flight time left, break out from the loop as the movie list is populated
			if(flightTime <= 0)
				break;
			
			// 4) if movie runtime falls within the flight time, add it to the set
			if(movie.getRuntime() <= flightTime)
			{
					recommendedMovieSet.add(movie);
					// reset the flight time to the remaining time
					flightTime = flightTime - movie.getRuntime();
			}
		}
		return recommendedMovieSet;
	}
	
	
	/**
	 * load the movies file from class path and return as string.
	 * @return json array
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private String getMovies() throws IOException, URISyntaxException
	{
		 return new String(Files.readAllBytes(Paths.get(getClass().getResource("/movies.json").toURI())));
	}
	
	public static void main(String[] args) throws IOException, URISyntaxException {
		Set<Movie> recommendedMovieSet = (new Recommendation(150)).getRecommendation();
		logger.log(Level.INFO, "---------------------------------------------");
		logger.log(Level.INFO, recommendedMovieSet.toString());
	}
	
}
