package com.skyhubdigital.assessment.recommendation.model;

/**
 * POJO with only the relevant fields for the recommendation 
 * Comparable for req. 'Prioritize longer movies over shorter ones (longer movies are always better right)'
 * @author Gaurav Wasan
 *
 */
public class Movie implements Comparable<Movie>{

	private String title;
	private int runtime;

	
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}

	public int getRuntime() {
		return runtime;
	}
	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + runtime;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie other = (Movie) obj;
		if (runtime != other.runtime)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Movie [title=" + title + ", runtime=" + runtime + "]";
	}
	public int compareTo(Movie m) {
		return Integer.compare(m.getRuntime(), runtime);
	}
	
	
}