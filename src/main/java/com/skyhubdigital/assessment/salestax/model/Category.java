package com.skyhubdigital.assessment.salestax.model;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.skyhubdigital.assessment.salestax.Tax;

/**
 * Everything to do with the Category and tax
 * Load the applicable tax values from properties file for flexibility
 * @author Gaurav Wasan
 *
 */
public class Category implements Tax{
	
	private String name;
	private GoodsClassification classification;
	static Logger logger = Logger.getLogger(Category.class.getName());
	
	private static final Properties properties = new Properties();

	static {
		URL url = ClassLoader.getSystemResource(PROPERTIES_CATEGORY_TAX);
		try {
			properties.load(url.openStream());
			logger.log(Level.INFO, "properties file loaded.");
		} catch (IOException e) {
			logger.log(Level.SEVERE, String.format("error while loading the properties file: ", e));
		}
	}
	
	/**
	 * Everything to do with the Category tax.
	 * Load the applicable tax values from properties file for flexibility
	 * @throws IOException 
	 */
	public Category(String name, GoodsClassification classification) throws IOException
	{
		this.name = name;
		this.classification = classification;
	}

	public BigDecimal getApplicableTax() {
		String defaultTax = properties.getProperty(OTHER);
		String duty = properties.getProperty(this.classification.name());
		String applicableTax = properties.getProperty(this.name, defaultTax);

		BigDecimal tax = new BigDecimal(applicableTax);
		tax = tax.add(new BigDecimal(duty));

		return tax;
	}

	@Override
	public String toString() {
		return "Category [name=" + name + ", classification=" + classification
				+ "]";
	}
	
	
	
	}