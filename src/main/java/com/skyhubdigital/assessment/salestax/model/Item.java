package com.skyhubdigital.assessment.salestax.model;

import java.math.BigDecimal;

import com.skyhubdigital.assessment.salestax.Tax;

/**
 * Item pojo
 * @author Gaurav Wasan
 *
 */
public class Item {
    private String name;
    private Category category;
    private BigDecimal quantity;
    private BigDecimal price;
    
    public Item(String name, Category category, BigDecimal quantity, BigDecimal price) {
        this.name = name;
        this.category = category;
        this.quantity = quantity;
        this.price = price;
    }
    
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public Category getCategory(){
        return category;
    }
    
    public void setCategory(Category category){
        this.category = category;
    }
    
    public BigDecimal getQuantity(){
        return quantity;
    }
    
    public void setQuantity(BigDecimal quantity){
        this.quantity = quantity;
    }
    
    public BigDecimal getPrice() {
        return price;
    }
    
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

	@Override
	public String toString() {
		return "Item [name=" + name + ", category=" + category + ", quantity=" + quantity + ", price=" + price + "]";
	}
	
	
    /**
     * Calculates applicable tax on the item
     * @param item
     * @return tax
     */
    public BigDecimal getTax(){
        BigDecimal salesTax = quantity.multiply(price).multiply(category.getApplicableTax()).divide(new BigDecimal("100"));
        return salesTax.divide(Tax.ROUNDING_SCALE).setScale(0, BigDecimal.ROUND_UP).multiply(Tax.ROUNDING_SCALE);
    }	
}