package com.skyhubdigital.assessment.salestax;

import java.math.BigDecimal;

/**
 * Interface for Category class
 * @author Gaurav Wasan
 *
 */
public interface Tax {

	static final BigDecimal ROUNDING_SCALE = new BigDecimal("0.05");
	static final String OTHER = "other";
	static final String PROPERTIES_CATEGORY_TAX = "categoryTax.properties";
	
	enum GoodsClassification{
        DOMESTIC,
        IMPORTED
    }
    
    public BigDecimal getApplicableTax();
}
