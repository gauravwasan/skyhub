package com.skyhubdigital.assessment.salestax;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.skyhubdigital.assessment.recommendation.Recommendation;
import com.skyhubdigital.assessment.salestax.model.Item;

/**
 * The basket class that abstracts the total and tax calculation from the caller
 * @author Gaurav Wasan
 *
 */
public class ShoppingCart {
	
	static Logger logger = Logger.getLogger(Recommendation.class.getName());
	private List<Item> shoppingItems;
    private BigDecimal subTotal = new BigDecimal(0);
    private BigDecimal salesTax = new BigDecimal(0);
    private BigDecimal total = new BigDecimal(0);
          
    public List<Item> getShoppingItems() {
    	logger.log(Level.INFO, shoppingItems.toString());
		return shoppingItems;
    }
    
    public void addShoppingItem(Item item) {
    	if(null == shoppingItems)
    	shoppingItems = new ArrayList<Item>();
    	
    	shoppingItems.add(item);
        calculateSubTotal(item);
        calculateSalesTax(item);
        calculateTotal();
    }
    
    public BigDecimal getSalesTax(){
        return salesTax;
    }
    
    public BigDecimal getTotal(){
        return total;
    }
    
    private void calculateSubTotal(Item item) {
        subTotal = subTotal.add(item.getQuantity().multiply(item.getPrice()));
    }
    
    private void calculateSalesTax(Item item) {
        salesTax = salesTax.add(item.getTax());
        /*logger.log(Level.INFO, String.format("salesTax %s", salesTax));*/
		
    }
    
    private void calculateTotal() {
        total = subTotal.add(salesTax);
    }
    
}