package com.skyhubdigital.assessment.brackets;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Simple not over architected validation class
 * 
You're working with an intern that keeps coming to you with JavaScript code that won't run because the braces, brackets, and parentheses are off.
To save you both some time, you decide to write a braces/brackets/parentheses validator.
'(', '{', '[' are called "openers." ')', '}', ']' are called "closers."
Write an efficient function that tells us whether or not an input string's openers and closers are properly nested.

Examples:
"{ [ ] ( ) }" should return true
"{ [ ( ] ) }" should return false
"{ [ }" should return false
 * @author Gaurav Wasan
 *
 */
public class Brackets {

	static Logger logger = Logger.getLogger(Brackets.class.getName());

	/**
	 * regex that excludes the required bracket types (){}[]
	 */
	final static String REGEX_BRACKETS_EXCLUDE = "[^(){}\\[\\]]";

	/**
	 * Function that does the validation
	 * @param input input string to be validated
	 * @return boolean response
	 */
	public boolean validator(String input) {
		// strip every other character from the string excluding the brackets
		String stripped = input.replaceAll(REGEX_BRACKETS_EXCLUDE, "");
		logger.log(Level.INFO, String.format("Stripped string %s", stripped));

		// 1)  validation on being balanced even before moving forward
		if (stripped.length() % 2 != 0) {
			logger.log(Level.INFO, "unbalanced number of opening and closing brackets");
			return false;
		}
		
		// 2) else move on and keep replacing all the open-close brackets with empty string
		while ((stripped.length() != 0)
				&& (stripped.contains("[]") || stripped.contains("()") || stripped .contains("{}"))) {
			
			stripped = stripped.replace("[]", "");
			stripped = stripped.replace("()", "");
			stripped = stripped.replace("{}", "");
		}
		
		// 3) length should be 0
		if (stripped.length() == 0) {
			logger.log(Level.INFO, "Valid input");
			return true;
		} else {
			logger.log(Level.INFO, "Invalid input");
			return false;
		}
	}
}
